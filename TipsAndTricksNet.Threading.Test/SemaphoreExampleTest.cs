﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TipsAndTricksNet.Threading.Test
{
    [TestFixture]
    public class SemaphoreExampleTest
    {
        [Test]
        public async Task shouldRunAsynchrous()
        {
            var semaphore1 = new SemaphoreSlim(1, 1);
            var semaphore2 = new SemaphoreSlim(1, 1);
            var controller1 = new SemaphoreExample("A", semaphore1);
            var controller2 = new SemaphoreExample("B", semaphore2);

            var tasks = new Task[]{ controller1.Run(), controller2.Run() };
            Stopwatch stopwatch = Stopwatch.StartNew();
            await Task.WhenAll(tasks);

            stopwatch.Elapsed.Should().BeLessThan(TimeSpan.FromSeconds(2));
            stopwatch.Elapsed.Should().BeGreaterThan(TimeSpan.FromSeconds(1));
        }

        [Test]
        public async Task shouldBlock()
        {
            var semaphore = new SemaphoreSlim(1, 1);
            var controller1 = new SemaphoreExample("A", semaphore);
            var controller2 = new SemaphoreExample("B", semaphore);

            var tasks = new Task[]{ controller1.Run(), controller2.Run() };

            Stopwatch stopwatch = Stopwatch.StartNew();   
            await Task.WhenAll(tasks);

            stopwatch.Elapsed.Should().BeGreaterThan(TimeSpan.FromSeconds(2));
        }
    }
}
