using FluentAssertions;
using System.Diagnostics;

namespace TipsAndTricksNet.Threading.Test
{
    [TestFixture]
    public class AwaitSemaphoreTest
    {
        [Test]
        public async Task shouldBeLessThan22Seconds()
        {
            AwaitSemaphore semaphore = new AwaitSemaphore();

            Stopwatch stopwatch = Stopwatch.StartNew();
            await semaphore.Synchronize();

            stopwatch.Elapsed.Should().BeLessThan(TimeSpan.FromSeconds(22));
            stopwatch.Elapsed.Should().BeGreaterThan(TimeSpan.FromSeconds(12));
        }
    }
}