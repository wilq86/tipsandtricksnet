﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipsAndTricksNet.Threading.Test
{
    [TestFixture]
    public class AsyncVoidTest
    {
        [Test]
        public void TestAsyncVoid()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            AsyncVoid asyncVoid = new AsyncVoid();
            asyncVoid.MethodAsync();

            stopwatch.Stop();
            stopwatch.Elapsed.TotalSeconds.Should().BeApproximately(1, 0.5);
        }
    }
}
