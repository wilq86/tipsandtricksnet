﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipsAndTrickNet.Test
{
    [TestFixture]
    public class DelegateVsEventTest
    {
        [Test]
        public void Test()
        {
            DelegateVsEvent dve = new DelegateVsEvent();

            dve.Action += dve.Function;
            dve.Action = dve.Function;
            dve.Action.Invoke(new object(), new EventArgs());

            dve.EventHandler += dve.Function;
            dve.EventHandler = dve.Function;
            dve.EventHandler.Invoke(new object(), new EventArgs());

            dve.EventEventHanlder += dve.Function;
            //Not possible - compile error
            //dve.EventEventHanlder = dve.Function;
            //Not possible - compile error
            //dve.EventEventHanlder.Invoke(null, null);
        }
    }
}
