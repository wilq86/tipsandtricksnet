using FluentAssertions;

namespace TipsAndTrickNet.Test
{
    [TestFixture]
    public class Tests
    {

        [Test]
        public void shouldWork()
        {
            NewDispose dispose= new NewDispose();

            dispose.Dispose();

            //Here is everything fine
            dispose._disposed.Should().Be(true);
            dispose._newDisposed.Should().Be(true);
        }

        [Test]
        public void shouldNotWork()
        {
            BaseDispose dispose = new NewDispose();

            dispose.Dispose();

            //Here is everything fine
            dispose._disposed.Should().Be(true);

            NewDispose newDispose = (NewDispose)dispose;

            //But here is not!!!
            newDispose._newDisposed.Should().Be(false);
        }
    }
}