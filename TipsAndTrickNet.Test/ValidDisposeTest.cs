﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipsAndTrickNet.Test
{
    [TestFixture]
    public class ValidDisposeTest
    {
        [Test]
        public void shouldWork()
        {
            NewValidDispose dispose = new NewValidDispose();

            dispose.Dispose();

            //Here is everything fine
            dispose._disposed.Should().Be(true);
            dispose._newDisposed.Should().Be(true);
        }

        [Test]
        public void shouldDisposeAllResources()
        {
            BaseValidDispose dispose = new NewValidDispose();

            dispose.Dispose();

            //Here is everything fine
            dispose._disposed.Should().Be(true);

            NewValidDispose newDispose = (NewValidDispose)dispose;

            newDispose._newDisposed.Should().Be(true);
        }
    }
}
