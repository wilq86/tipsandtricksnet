﻿using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace TipsAndTrickNet.Test
{
    [TestFixture]
    public class StringInternTest
    {

        [Test]
        public void shouldCheckReferences() {

            string s1 = "MyTest";
            string s2 = new StringBuilder().Append("My").Append("Test").ToString();
            string s3 = string.Intern(s2);

            s1.Should().Be(s2).And.Be(s3);

            ((Object)s1 == (Object)s2).Should().BeFalse();
            ((Object)s1 == (Object)s3).Should().BeTrue();

            Object.ReferenceEquals(s1, s2).Should().BeFalse(); 
            Object.ReferenceEquals(s1, s3).Should().BeTrue();


            string s0 = string.Intern("fkdasjlfsa");
        }

        [Test]
        public void shouldBeDiffrentReferences()
        {
            string s1 = "My";
            string s2 = "Test";

            string s3 = s1 + s2;
            string s4 = "MyTest";

            ((Object)s3 == (Object)s4).Should().BeFalse();

            Object.ReferenceEquals(s3, s4).Should().BeFalse();
        }

        [Test]
        public void shouldBeThisSameReferences()
        {
            const string s1 = "My";
            const string s2 = "Test";

            string s3 = s1 + s2;
            string s4 = "MyTest";

            ((Object)s3 == (Object)s4).Should().BeTrue();

            Object.ReferenceEquals(s3, s4).Should().BeTrue();
        }
    }
}
