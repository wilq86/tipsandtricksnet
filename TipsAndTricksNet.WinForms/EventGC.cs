﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TipsAndTricksNet
{
    public class Publisher
    {
        public event EventHandler<EventArgs> Call;

        public Publisher()
        {
            //Task.Run(() =>
            //{
            //    while (true)
            //    {
            //        Thread.Sleep(TimeSpan.FromSeconds(1));
            //        Call(this, new EventArgs());
            //    }
            //});
        }

        ~Publisher()
        {

        }
    }

    public class Subscriber
    {
        public void event_Call(object sender, EventArgs e)
        {
            Console.WriteLine(DateTime.Now.ToString());
        }
    }

    public class EventGC
    {
        static void Main()
        {
            if (true == true)
            {
                {
                    var @event = new Publisher();
                    @event.Call += new Subscriber().event_Call;
                }
            }

            Console.ReadKey();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            Console.ReadKey();
        }

        
    }
}
