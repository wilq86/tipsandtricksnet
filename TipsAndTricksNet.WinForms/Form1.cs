﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TipsAndTricksNet.WinForms
{
    public partial class Form1 : Form
    {
        private Thread Thread;
        private System.Threading.Timer Timer;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Thread = Thread.CurrentThread;
            Timer = new System.Threading.Timer(TimerCallback, null, TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(10));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            while (true)
            {
                Thread.Sleep(1000);
            }
        }

        private void TimerCallback(object state)
        {
            StackTrace trace = GetStackTrace(Thread);
            string filePath = $"{DateTime.Now.ToString("yyyy-MM-dd_HH_mm_ss_fff")}.txt";
            File.WriteAllText(filePath, trace.ToString());
        }

        private StackTrace GetStackTrace(Thread targetThread)
        {
            StackTrace stackTrace = null;
            var ready = new ManualResetEventSlim();

            new Thread(() =>
            {
                // Backstop to release thread in case of deadlock:
                ready.Set();
                Thread.Sleep(200);
                try { targetThread.Resume(); } catch { }
            }).Start();

            ready.Wait();
            targetThread.Suspend();
            try { stackTrace = new StackTrace(targetThread, true); }
            catch { /* Deadlock */ }
            finally
            {
                try { targetThread.Resume(); }
                catch { stackTrace = null;  /* Deadlock */  }
            }

            return stackTrace;
        }
    }
}
