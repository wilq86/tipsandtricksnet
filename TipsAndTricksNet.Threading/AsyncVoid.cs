﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipsAndTricksNet.Threading
{
    public class AsyncVoid
    {
        public async void MethodAsync()
        {
            Thread.Sleep(1000);
            await Task.Delay(1000);
        }
    }
}
