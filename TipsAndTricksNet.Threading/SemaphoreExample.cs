﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipsAndTricksNet.Threading
{
    public class SemaphoreExample
    {
        private SemaphoreSlim Semaphore;
        private string Name;

        public SemaphoreExample(string name, SemaphoreSlim semaphore)
        {
            Semaphore = semaphore;
            Name = name;
        }

        public async Task Run()
        {
            for (int i = 1; i <= 10; i++)
            {
                _ = await DoSomeTaskAsync(i);
            }
        }

        async Task<bool> DoSomeTaskAsync(int i)
        {
            try
            {
                await Semaphore.WaitAsync();
                WriteLine("Success: " + i + " is Doing its work");
                _ = await Task.Run(() => DoNothing(i));
                WriteLine(i + " Exit.");
            }
            finally
            {
                Semaphore.Release();
            }
            return true;
        }

        private void WriteLine(string v) => Console.WriteLine($"{Name}:{v}");   

        string DoNothing(int i)
        {
            WriteLine("Starting to do nothing for " + i);
            Thread.Sleep(100);
            WriteLine("DoNothing " + i);
            return "";
        }
    }
}
