﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipsAndTricksNet.Threading
{
    public class AwaitSemaphore
    {
        private SemaphoreSlim SemaphoreSlim = new SemaphoreSlim(1, 1);
       
        async Task Method1()
        {
            await Task.Delay(TimeSpan.FromSeconds(10));
            //var data = SomeService.GetData();

            await SemaphoreSlim.WaitAsync();
            try
            {
                await Task.Delay(TimeSpan.FromSeconds(1));
                //_ctx.SomeCollection.Add(entity);
                //await _ctx.Save();
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        async Task Method2()
        {
            await Task.Delay(TimeSpan.FromSeconds(10));
            //var data = SomeService.GetData();

            await SemaphoreSlim.WaitAsync();
            try
            {
                await Task.Delay(TimeSpan.FromSeconds(1));
                //_ctx.SomeCollection.Add(entity);
                //await _ctx.Save();
            }
            finally
            {
                SemaphoreSlim.Release();
            }
        }

        public async Task Synchronize()
        {
            Stopwatch stopwatch = Stopwatch.StartNew(); 
            var tasks = new Task[] { Method1(), Method2() };

            await Task.WhenAll(tasks);
            Console.WriteLine(stopwatch.Elapsed);
        }
    }
}
