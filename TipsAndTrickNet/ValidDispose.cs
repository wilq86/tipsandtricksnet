﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipsAndTrickNet
{
    public class BaseValidDispose : IDisposable
    {
        public bool _disposed = false;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    
                }

                _disposed = true;
            }
        }
    }

    public class NewValidDispose : BaseValidDispose
    {
        public bool _newDisposed = false;

        protected override void Dispose(bool disposing)
        {
            if (!_newDisposed)
            {
                if (disposing)
                {

                }

                _newDisposed = true;
            }

            base.Dispose(disposing);
        }
    }
}
