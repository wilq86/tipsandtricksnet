﻿namespace TipsAndTrickNet
{

    public class BaseDispose : IDisposable
    {
        public bool _disposed = false;

        public void Dispose()
        {
            _disposed = true;
        }
    }

    public class NewDispose : BaseDispose, IDisposable
    {
        public bool _newDisposed = false;

        //Invalid
        public new void Dispose()
        {
            _newDisposed = true;
            base.Dispose();
        }

    }
}