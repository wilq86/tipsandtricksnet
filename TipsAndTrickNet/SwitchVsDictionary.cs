﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipsAndTrickNet
{
    public enum BigEnum
    {
        one = 1,
        two,
        three,
        four,
        five,
        six,
        seven,
        eight,
        nine,
        ten,
        eleven,
        twelve,
        thirteen,
        fourteen,
        fifteen,
        sixteen,
        seventeen,
        eighteen,
        nineteen,
        twenty,
        twenty_one,
        twenty_two,
        twenty_three,
        twenty_four,
        twenty_five,
        twenty_six,
        twenty_seven,
        twenty_eight,
        twenty_nine,
        thirty,
        thirty_one,
        thirty_two,
        thirty_three,
        thirty_four,
        thirty_five,
        thirty_six,
        thirty_seven,
        thirty_eight,
        thirty_nine,
        forty,
        forty_one,
        forty_two,
        forty_three,
        forty_four,
        forty_five,
        forty_six,
        forty_seven,
        forty_eight,
        forty_nine,
        fifty,
        fifty_one,
        fifty_two,
        fifty_three,
        fifty_four,
        fifty_five,
        fifty_six,
        fifty_seven,
        fifty_eight,
        fifty_nine,
        sixty,
        sixty_one,
        sixty_two,
        sixty_three,
        sixty_four,
        sixty_five,
        sixty_six,
        sixty_seven,
        sixty_eight,
        sixty_nine,
        seventy,
        seventy_one,
        seventy_two,
        seventy_three,
        seventy_four,
        seventy_five,
        seventy_six,
        seventy_seven,
        seventy_eight,
        seventy_nine,
        eighty,
        eightyone,
        eighty_two,
        eighty_three,
        eighty_four,
        eighty_five,
        eighty_six,
        eighty_seven,
        eighty_eight,
        eighty_nine,
        ninety,
        ninety_one,
        ninety_two,
        ninety_three,
        ninety_four,
        ninety_five,
        ninety_six,
        ninety_seven,
        ninety_eight,
        ninety_nine,
        one_hundred,
    }

    public class SwitchVsDictionary
    {
        private Dictionary<string, int> Dictionary = new Dictionary<string, int>();

        public SwitchVsDictionary() {

            var values = (BigEnum[])Enum.GetValues(typeof(BigEnum));
            Dictionary = values.ToDictionary(x => x.ToString(), x => (int)x);
        } 
        public int Switch()
        {

        }
    }
}
