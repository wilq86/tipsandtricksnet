﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipsAndTrickNet
{
    public class DelegateVsEvent
    {
        public event EventHandler<EventArgs>? EventEventHanlder;

        public EventHandler<EventArgs>? EventHandler;

        public Action<object, EventArgs>? Action;

        public event EventHandler<EventArgs> MyEvent
        {
            add
            {
                // code here adds the incoming delegate instance to underlying list of 
                // event handlers
                Console.WriteLine();
            }
            remove
            {
                // code here removes the delegate instance from the underlying list of 
                // event handlers
            }
        }

        public void Function(object? sender, EventArgs e) { }    
    }
}
