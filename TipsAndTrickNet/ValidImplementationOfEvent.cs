﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TipsAndTrickNet
{
    public class ValidImplementationOfEvent
    {
        public class A
        {
            public event EventHandler<EventArgs>? Event;

            public void Method()
            {
                OnEvent();
            }

            protected void OnEvent()
            {
                Event?.Invoke(this, EventArgs.Empty);
            }
        }

        public class B : A 
        {
            public void AnotherMethod()
            {
                //Not possible - compilation error
                //Event.Invoke(this, EventArgs.Empty);    
                OnEvent();
            }
        }
    }

}
